Source: jctools
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk-headless,
               junit4,
               libexec-maven-plugin-java,
               libguava-testlib-java,
               libjavaparser-java,
               libmaven-bundle-plugin-java,
               maven-debian-helper
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/java-team/jctools
Vcs-Git: https://salsa.debian.org/java-team/jctools.git
Homepage: https://jctools.github.io/JCTools/

Package: libjctools-java
Architecture: all
Multi-Arch: foreign
Depends: ${maven:Depends},
         ${misc:Depends}
Suggests: ${maven:OptionalDepends}
Description: Java Concurrency Tools for the JVM
 JCTools offers some concurrent data structures currently missing from the JDK:
  * SPSC/MPSC/SPMC/MPMC variations for concurrent queues:
    * SPSC - Single Producer Single Consumer (Wait Free, bounded and unbounded)
    * MPSC - Multi Producer Single Consumer (Lock less, bounded and unbounded)
    * SPMC - Single Producer Multi Consumer (Lock less, bounded)
    * MPMC - Multi Producer Multi Consumer (Lock less, bounded)
  * An expanded queue interface (MessagePassingQueue):
    * relaxedOffer/Peek/Poll: trade off conflated guarantee on full/empty queue
      state with improved performance.
    * drain/fill: batch read and write methods for increased throughput
      and reduced contention
